# README #

A repository used to store a sample React Native application. This is for demonstration purposes and learning purposes only.

### What is this repository for? ###

* A quick example of using React Native
* [Learn React Native](https://facebook.github.io/react-native/docs/getting-started.html)
* [Learn React Native Components](https://react-native-training.github.io/react-native-elements/)

### How do I get set up? ###

* Install [Expo](https://expo.io/) on your desktop and mobile device
* Install [NodeJS](https://nodejs.org/en/download/)
* Add Node to the PATH variable
* Run `npm install -g create-react-native-app` from the terminal
	* Root privileges may be required 
* Run `create-react-native-app AwesomeProject`
	* Here `AwesomeProject` is the name of the project you are about to create
	* If it fails run `sudo sysctl -w kern.maxfilesperproc=524288`
	* If it still failed run `sudo sysctl -w kern.maxfiles=5242880`
* `cd AwesomeProject`
* Run `npm start` from within the `AwesomeProject`
	* Now if you have iOS emulator setup you can render the iOS version of the App
	* Now if you have Android emulator setup you can render the Android version of the App
* If you want to render a live preview from your phone start Expo from your phone and scan the QR code generated from `npm start` this will render the react native app in your phone

### Making the Application (Android) ###

Using expo we can generate a standalone APK file that can be run on an [Android device](https://docs.expo.io/versions/latest/guides/building-standalone-apps.html)

* Run the following command on your system `sudo npm install -g exp`
* Next you should edit the app.json file to include the necessary metadata that is needed for your application to display
* Next open a terminal and launch two seperate tabs
* The first tab run `exp start`
  * It could be possible that this throws an error, you may have to login first running `exp login`
* In the second tab run exp build:android
  * This will launch an interactive session and ask you some questions
  		* Do you want to let **expo** do the build? -> **Yes**
  		* Do you have a keystore that you want **expo** to use? -> Not needed
  * This will start the expo build process, it is asynchronous so the console will be released to you eventually
  * To track the build process run `exp build:status`
  		* If it is not ready it will respond back to you with *Android: Build in progress...*
  		* If it is ready it will respond back to you with *APK: https://exp-shell-app-assets.s3-us-west-1.amazonaws.com/android%2F%40...-signed.apk*
  * Click the link and you will download your APK file to your server for download
  * **Note:** I have uploaded a sample build to my phone and it works perfectly


### React Native Component ###

For information on the public *React Native* components navigate to the following documentation to see what is [supported natively](https://facebook.github.io/react-native/docs/getting-started.html).


### Who do I talk to? ###

* [Patrique Legault](mailto:patrique.legault@gmail.com)
