import React from 'react';
import { StyleSheet, ScrollView, View } from 'react-native';
import { Container, Content, Picker, Button, Text } from "native-base";
import Expo from "expo";

import WeatherForm from './components/weather/WeatherForm';
import MainComponent from './components/main/MainComponent';

export default class App extends React.Component {

  constructor() {
    super();
    this.state = {
      isReady: false
    };
  }

  // This is required for the app to run since the import is using custom fonts
  async componentWillMount() {

      await Expo.Font.loadAsync({
        Roboto: require("native-base/Fonts/Roboto.ttf"),
        Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
        Ionicons: require("native-base/Fonts/Ionicons.ttf")
      });

     this.setState({ isReady: true });
  }

  render() {

    if (!this.state.isReady) {
      return <Expo.AppLoading />;
     }

    return (
      <MainComponent /> 
    );

  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1
  },
  headerContainer: {
    flexDirection: 'row',
    height: 100,
    backgroundColor: '#fff',
    alignItems: 'center'
  }
});