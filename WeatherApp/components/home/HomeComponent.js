import React from "react";
import { StyleSheet, StatusBar, ScrollView, View , Image, Text} from "react-native";
import { Button, Container, Card, CardItem, Body, Content, Header, Title, Left, Icon, Right } from "native-base";

import Logo from '../../assets/img/logo.png';

export default class HomeComponent extends React.Component {
  render() {

    return (

      <ScrollView keyboardShouldPersistTaps="always" keyboardDismissMode='on-drag'>
        <Container>
            <Header style={styles.textAffect}>
                <Left>
                  <Button
                    transparent
                    onPress={() => this.props.navigation.navigate("DrawerOpen")}
                  >
                    <Icon name="menu" />
                  </Button>
                </Left>
                <Body>
                  <Title>Welcome</Title>
                </Body>
                <Right />
              </Header>

              <Content padder>
                  <Card style={styles.mainContainer}>
                    <CardItem>
                    	<View>
                    		<Image 
                    			source={Logo}
  							/>
						</View>
                    </CardItem>
                    <CardItem>
                    	<Text style={styles.textContainer}>The Legault.Tech Mobile App!</Text>
                    </CardItem>
                  </Card>
              </Content>
        </Container>
      </ScrollView>

    );
  }
}

const styles = StyleSheet.create({
  textAffect: {
    paddingTop: 50,
    paddingBottom: 30 
  },
  mainContainer: {
    alignItems: 'center',
  },
  textContainer: {
    fontSize: 25,
    fontWeight: 'bold',
    textDecorationLine: 'underline'
  }
});
