import React, { Component } from 'react';
import {Image, StyleSheet} from 'react-native';
import Logo from '../../assets/img/logo.png';

const styles = StyleSheet.create({
  stretch: {
    width: 60,
    height: 60,
    top : 20
  }
});

export default class LogoComponent extends Component {
	render() {
	    return (
	    		<Image
	    		 	style={styles.stretch}
				  	source={Logo}
				/>
	    );
    }
}
