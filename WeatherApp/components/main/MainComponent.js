import React, { Component } from "react";
import { DrawerNavigator } from "react-navigation";

import SideBar from "../sideBar/SideBar.js";

import HomeComponent from '../home/HomeComponent';
import WeatherScreen from '../weather/WeatherScreen';



// This component is where the home page is loaded from http://docs.nativebase.io/docs/examples/navigation/StackNavigationExample.html

const HomeScreenRouter = DrawerNavigator(
  {
  	//The name of the variable "Home" must be the same as the SideBar.js routes variable name
  	Home: {screen: HomeComponent},
    Weather: { screen: WeatherScreen }
  },
  {
    contentComponent: props => <SideBar {...props} />
  }
);

export default HomeScreenRouter;