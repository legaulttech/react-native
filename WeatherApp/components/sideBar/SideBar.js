import React from "react";
import { StyleSheet, AppRegistry, Image, StatusBar } from "react-native";
import { Button, Text, Container, List, ListItem, Content, Icon } from "native-base";

const routes = ["Home", "Weather"];

export default class SideBar extends React.Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(data) {
    this.props.navigation.navigate(data);
  }


  render() {
    return (
      <Container>
        <Content>
          <List
            dataArray={routes}
            renderRow={data => {
              return (
                <ListItem
                  button={true}
                  onPress={() => this.handleClick(data)}
                >
                  <Text style={styles.textAffect}>{data}</Text>
                </ListItem>
              );
            }}
          >
          </List>
        </Content>
      </Container>
    );
  }
}


const styles = StyleSheet.create({
  textAffect: {
    marginTop: 25,
    fontSize: 20,
    fontWeight: 'bold'
  }
});