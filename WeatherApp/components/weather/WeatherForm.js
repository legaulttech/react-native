import React, { Component } from 'react';
import {StyleSheet, View, Picker, Text} from 'react-native';
import {Button, FormLabel, FormInput} from 'react-native-elements';

//Routing API's
import { Body } from "native-base";

//Networking API's
import axios from 'axios';

//Custom components
import WeatherResult from './formBeans/WeatherResult';

export default class WeatherForm extends Component {

	constructor(props)
	{
	    super(props);

	    this.state = {
	      country: '',
	      city: '',
	      weatherData: '',
	      title: 'Get Weather',
	      appId: ''
	    }

	    this.getWeather = this.getWeather.bind(this);
  	}

	getWeather() {

		{/*For rendering to work accordingly need to set the state https://stackoverflow.com/questions/44705967/react-native-rendering-component-on-button-click */}
    	
    	if(this.state.title == "Clear")
    	{
    		this.setState({country: '', city: '', weatherData: '', title: 'Get Weather'})
    	}
    	else
    	{
    		if(this.state.appId != '')
    		{
    			var query = this.state.city + ',' + this.state.country;
    			
				axios.get('http://api.openweathermap.org/data/2.5/weather', {
		    	params: {
		      		q: query,
		      		appid: this.state.appId
		    	}
			  	})
				.then((response) => {
					this.setState({city: '', weatherData: JSON.stringify(response), title: 'Clear'});
				})
				.catch(function (error) {
					console.log(error);
				});
			}
			else
			{
				this.setState({country: '', city: '', title: 'Get Weather', weatherData : '0'});
			}
    		
		}
 	}

	render() {

	    return (

  			<Body
  				style={styles.body}
  			>

  				<Text style={styles.labelSize}>App ID</Text>
    			<Picker style={styles.pickerView}
				  selectedValue={this.state.appId}
				  onValueChange={(itemValue) => this.setState({appId: itemValue})}>
				  <Picker.Item label="Select an App ID" value="0" />
				  <Picker.Item label="Open Weather Map" value="b1b15e88fa797225412429c1c50c122a1" />
				  <Picker.Item label="Personal" value="b5a10e29cc003a75f2931221b35926ac" />
				</Picker>

				<Text style={styles.labelSize}>Country</Text>
    			<Picker style={styles.pickerView}
				  selectedValue={this.state.country}
				  onValueChange={(itemValue) => this.setState({country: itemValue})}>
				  <Picker.Item label="Select a Country" value="0" />
				  <Picker.Item label="Canada" value="can" />
				  <Picker.Item label="United States" value="us" />
				  <Picker.Item label="Germany" value="deu" />
				</Picker>

    			<Text style={styles.labelSize}>City</Text>
		        <Picker style={styles.pickerView}
				  selectedValue={this.state.city}
				  onValueChange={(itemValue) => this.setState({city: itemValue})}>
				  <Picker.Item label="Select a City" value="0" />
				  <Picker.Item label="Sudbury" value="Sudbury" />
				  <Picker.Item label="Ottawa" value="Ottawa" />
				  <Picker.Item label="Berlin" value="Berlin" />
				</Picker>
			  	
			  	<Button
			  		onPress={() => this.getWeather()}
			  		accessibilityLabel="Retrieve the city weather"
					buttonStyle={{backgroundColor: 'blue', borderRadius: 10}}
					textStyle={{textAlign: 'center', fontSize: 20}}
					title={this.state.title}
				/>

				<View style={styles.weatherContainer}>
					<WeatherResult result={this.state.weatherData} />
				</View>
			</Body>
	    );
    }
}

const styles = StyleSheet.create({
  body: {
  	alignItems:'center' 
  },
  weatherContainer: {
    height: 400,
    backgroundColor: '#fff',
    top: 10
  },
  pickerView: {
    width: 210,
    left: 10,
    marginBottom: 20
  },
  formInput: {
    width: 300,
    marginBottom: 20,
    textAlign : 'center',
    fontWeight: 'bold',
    fontSize: 20
  },
  labelSize: {
    fontSize: 30,
    fontWeight: 'bold'
  }
});