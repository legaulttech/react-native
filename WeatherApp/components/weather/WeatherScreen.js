import React from "react";
import { StyleSheet, StatusBar, ScrollView } from "react-native";
import WeatherForm from './WeatherForm';
import { Button, Text, Container, Card, CardItem, Body, Content, Header, Title, Left, Icon, Right } from "native-base";

export default class HomeScreen extends React.Component {
  render() {

    return (

      <ScrollView keyboardShouldPersistTaps="always" keyboardDismissMode='on-drag'>
        <Container>
            <Header style={styles.textAffect}>
                <Left>
                  <Button
                    transparent
                    onPress={() => this.props.navigation.navigate("DrawerOpen")}
                  >
                    <Icon name="menu" />
                  </Button>
                </Left>
                <Body>
                  <Title>Local Weather</Title>
                </Body>
                <Right />
              </Header>

              <Content padder>
                  <Card>
                    <CardItem>
                      <WeatherForm />
                    </CardItem>
                  </Card>
              </Content>
        </Container>
      </ScrollView>

    );
  }
}

const styles = StyleSheet.create({
  textAffect: {
    paddingTop: 50,
    paddingBottom: 30 
  }
});
