import React, { Component } from 'react';
import {StyleSheet, View} from 'react-native';
import {Text} from 'react-native-elements';

export default class WeatherResult extends Component {

	constructor(props)
	{
	    super(props);

	    this.getData = this.getData.bind(this);
  	}

  	getData() {

  		var resultString = "No Weather Selected";

  		if(this.props.result != "" && this.props.result != "0")
  		{
  			resultString = "The weather in ";
  			var resultTemp = JSON.parse(this.props.result);


  			resultString = resultString + resultTemp.data.name + " is ";

  			resultString = resultString + (resultTemp.data.main.temp - 273.15).toFixed(2) + ' °C';
   		}

   		if(this.props.result == "0")
   		{
   			resultString = "Please select an AppID";
   		}

		return resultString;

  	}
  		

	render() {
		return(
			<View style={styles.weatherContainer}>
				<Text h4 style={styles.textContainer}>{this.getData()}</Text>
			</View>
		);
	}
}

const styles = StyleSheet.create({
  weatherContainer: {
  	top: 30,
    height: 300,
    backgroundColor: '#fff',
    alignItems: 'center'
  },
   textContainer: {
    textAlign : 'center'
  }
});